// file      : common/pragma/driver.cxx
// license   : GNU GPL v2; see accompanying LICENSE file

// Test #pragma db parsing.
//

#include <memory>
#include <cassert>
#include <iostream>

#include <odb/exceptions.hxx>
#include <odb/transaction.hxx>

#include <common/common.hxx>

#include "test.hxx"
#include "test-odb.hxx"

using namespace std;
using namespace odb::core;

int
main ()
{
}
